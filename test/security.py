#!/usr/bin/env python3
import sys

if len(sys.argv) > 1:
    DOCKERFILE=sys.argv[1]
else:
    sys.exit("[e] Usage: "+sys.argv[0]+"[Dockerfile path]")

if '/var/run/docker.sock:/var/run/docker.sock' in open(DOCKERFILE).read():
    sys.exit("[e] docker.sock is mounted inside the docker")
